import { useState, useCallback } from 'react';

export default function useChangeHandler({ validate, init, required } = {}) {
  const V = validate || (required ? (v) => !v && 'required' : () => {});

  const [{ value, error }, setValueAndError] = useState({ value: init, error: V(init) });

  const changeHandler = useCallback(
    (ev) => {
      if (!ev || !ev.target) {
        console.warn('changeHandler called without ev/target');
        return;
      }
      const nextValue = (!ev.target.value || ev.target.value === '') ? undefined : ev.target.value;
      setValueAndError({ value: nextValue, error: ev.target.validationMessage || V(nextValue) });
    },
    []
  );

  return [value, changeHandler, error];
}