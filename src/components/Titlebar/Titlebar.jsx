import { useContext } from "react";
import { Alignment, Button, Classes, Drawer, DrawerSize, Navbar } from "@blueprintjs/core"
import { Link, NavLink,  } from "react-router-dom"
import { ResponsiveContext } from "../App"
import './titlebar.less'
import useToggle from "../../useToggle";

const links = [
  { to: '/', text: 'Home', icon: 'home' },
  { to: '/works', text: 'Works', icon: 'book' },
  // { to: '/about-me', text: 'About Emily', icon: 'person' },
  { to: '/newsletter', text: 'Newsletter', icon: 'envelope' },
  { to: '/contact', text: 'Contact', icon: 'social-media' },
];

function focusMain() {
  setTimeout(
    () => { document.getElementsByTagName('main')[0].focus(); },
    10
  );
}

export default function Titlebar() {
  const { isMobile, isTablet, isPortrait } = useContext(ResponsiveContext);
  const [isNavDrawerOpen, toggleNavDrawer] = useToggle(false);

  const Links = links.map((link) => (
    <NavLink
      key={link.to}
      to={link.to}
      className={({ isActive }) => `nav-button ${Classes.BUTTON} ${Classes.MINIMAL} ${
        Classes.ICON}-${link.icon} ${isActive ? Classes.ACTIVE : ''}`}
      onClick={toggleNavDrawer.FALSE}
    >
      {link.text}
    </NavLink>
  ));

  return (
    <header className="titlebar">
      <Button className="skip-to-content" onClick={focusMain} text="Skip to content" />
      <nav className={`navbar ${Classes.NAVBAR}`}>
        <Navbar.Group className="logo-container" align={Alignment.LEFT}>
          <Navbar.Heading className="logo"><Link to="/">Emily L. Finch</Link></Navbar.Heading>
        </Navbar.Group>

        <Navbar.Group align={Alignment.RIGHT} className="nav-links">
          {isMobile || isTablet ? (
            <>
              <Button
                icon="menu"
                aria-label="open navigation menu"
                minimal
                onClick={toggleNavDrawer}
              />
              <Drawer
                isOpen={isNavDrawerOpen}
                onClose={toggleNavDrawer.FALSE}
                className="nav-links nav-drawer"
                title=""
              >
                {Links}
              </Drawer>
            </>
          ) : Links
          }
        </Navbar.Group>
      </nav>
    </header>
  );
}