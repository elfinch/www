import { Card, Elevation } from "@blueprintjs/core";
import * as images from '../../../static/images'
import './contact.less'

const socialMedia = require('../../../static/data/socialMedia.json');

export default function Contact() {

  return (
    <main tabIndex={-1} className="contact">
      <h1>Contact</h1>
      <Card elevation={Elevation.TWO}>
        <h2>Connect with Emily L. Finch on Social Media</h2>
        {Object.entries(socialMedia).map(([medium, meta]) => (
          <div key={medium} className="medium">
            <a href={meta.url}>
              <img src={images[meta.image]} alt={medium} />
              <span className="handle">{meta.text}</span>
            </a> 
          </div>
        ))}
      </Card>
      <hr/>
      <iframe
        src="https://us14.list-manage.com/contact-form?u=e12c33b9577c48d9fc701754d&form_id=45daf276457536d2e43e7484f2da7e29"
      />
    </main>
  )
}