import { lazy, createContext, useEffect, Suspense } from 'react';
import { useMediaQuery } from 'react-responsive'
import { Route, Routes } from 'react-router-dom'

const AboutMe = lazy(() => import('./AboutMe'));
const Contact = lazy(() => import('./Contact'));
const Home = lazy(() => import('./Home'));
const Newsletter = lazy(() => import('./Newsletter'));
const Titlebar = lazy(() => import('./Titlebar'));
const Works = lazy(() => import('./Works'));

export const ResponsiveContext = createContext({});

export default function App() {
  const responsiveValue = {
    isDesktop: useMediaQuery({ query: '(min-width: 1224px)' }),
    isTablet: useMediaQuery({ query: '(max-width: 1224px) and (min-width: 600px)' }),
    isMobile: useMediaQuery({ query: '(max-width: 600px)' }),
    isPortrait: useMediaQuery({ query: '(orientation: portrait)' }),
  };

  const className = [
    responsiveValue.isMobile && 'mobile', 
    responsiveValue.isPortrait && 'portrait',
    responsiveValue.isTablet && 'tablet', 
    responsiveValue.isDesktop && 'desktop',
  ].filter((c) => c).join(' ');

  return (
    <ResponsiveContext.Provider value={responsiveValue}>
      <div className={className}>
        <Suspense fallback={null}><Titlebar /></Suspense>
        <Suspense fallback={null}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/works" element={<Works />} />
            <Route path="/about-me" element={<AboutMe />} />
            <Route path="/newsletter" element={<Newsletter />} />
            <Route path="/contact" element={<Contact />} />
          </Routes>
        </Suspense>
      </div>
    </ResponsiveContext.Provider>
  );
}