import { useContext } from "react";
import { ResponsiveContext } from "../App";

export default function BookCover({ image }) {

  const { isMobile } = useContext(ResponsiveContext);
  
  return (
    <img
      {...image}
      src={`https://res.cloudinary.com/djihjhtci/image/upload/f_auto,q_auto:best,w_${isMobile ? '496' : '200'}/emilylfinch/www/${image.src}`}
    />
  )
}