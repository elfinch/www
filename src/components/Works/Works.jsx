import { AnchorButton, Card, Elevation } from "@blueprintjs/core";
import { marked } from "marked";
import "./works.less"
import BookCover from "../shared/BookCover";

const works = require('../../../static/data/works');

export default function Works() {

  return (
    <main tabIndex={-1}>
      <h1>Works by Emily L. Finch</h1>
      {works.map((work) => (
        <article key={work.title} className="work">
          <Card elevation={Elevation.TWO}>
            <h2 className="book-title">{work.title}</h2>
            {work.subtitle && <p className="book-subtitle">{work.subtitle}</p>}
            <div className="details">
              {work.image && (
                <a href={work.links[0].href} target="_blank">
                  <BookCover image={work.image} />
                </a>
              )}
              <div className="rhs">
                <div dangerouslySetInnerHTML={{ __html: marked.parse(work.brief) }} />
                <div className="links-container">
                  {work.links.map((link) => <AnchorButton key={link.href} target="_blank" {...link} />)}
                </div>
              </div>
            </div>
          </Card>
        </article>
      ))}
    </main>
  )
}