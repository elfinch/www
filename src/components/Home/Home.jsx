import { lazy, Suspense } from 'react';

const NewsItem = lazy(() => import('./NewsItem'));
const news = require('../../../static/data/news');

export default function Home() {

  return (
    <main tabIndex={-1}>
      {news && news.length && (
        <section>
          <h1>News</h1>
          <Suspense fallback={null}>
            {news.map((newsitem) => <NewsItem {...newsitem} key={newsitem.headline} />)}
          </Suspense>
        </section>
      )}
    </main>
  );
}