import { AnchorButton, Card, Elevation } from "@blueprintjs/core"
import { marked } from 'marked'
import './newsitem.less'
import BookCover from "../shared/BookCover";

export default function NewsItem({ links, image, ...articleProps }) {

  const headline = articleProps.headline && marked.parse(articleProps.headline);
  const body = marked.parse(articleProps.body);

  return (
    <article className="newsitem">
      <Card elevation={Elevation.TWO}>
        <h2 dangerouslySetInnerHTML={{ __html: headline }} />
        <div className={`content`}>
          {image && (
            <div>
              <BookCover image={image} />
            </div>
          )}
          <div className="rhs">
            <div dangerouslySetInnerHTML={{ __html: body }} />
            {links && (
              <div className="links-container">
                {links.map((link) => <AnchorButton key={link.href} target="_blank" {...link} />)}
              </div>
            )}
          </div>
        </div>
      </Card>
    </article>
  );
}
