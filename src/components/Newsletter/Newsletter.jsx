import { useContext } from "react";
import { Button, Card, Classes, Elevation } from "@blueprintjs/core"
import './newsletter.less'
import { ResponsiveContext } from "../App"

export default function Newsletter() {
  const { isTabletOrMobile } = useContext(ResponsiveContext);

  return (
    <main id="newsletter">
      <h1>Newsletter</h1>
      <Card elevation={Elevation.TWO} className={`newsletter-container ${isTabletOrMobile ? 'fullwidth' : ''}`}>
        <p>Subscribe to the newsletter to be informed about upcoming releases.</p>
        <div id="mc_embed_signup">
          <form 
            action="https://emilylfinch.us14.list-manage.com/subscribe/post?u=e12c33b9577c48d9fc701754d&amp;id=487ca94ed4" 
            method="post" 
            id="mc-embedded-subscribe-form" 
            name="mc-embedded-subscribe-form" 
            target="_blank"
            noValidate
          >
            <div id="mc_embed_signup_scroll">
              <div className={Classes.FORM_GROUP}>
                <label htmlFor="mce-EMAIL" className={Classes.LABEL}>
                  Email Address <span className={Classes.TEXT_MUTED}>(required)</span>
                </label>
                <input 
                  type="email"
                  name="EMAIL" 
                  id="mce-EMAIL" 
                  className={`${Classes.INPUT} ${isTabletOrMobile ? Classes.FILL : ''}`}
                />
              </div>
              <div className={Classes.FORM_GROUP}>
                <label htmlFor="mce-NAME" className={Classes.LABEL}>
                  Name <span className={Classes.TEXT_MUTED}>(required)</span>
                </label>
                <input
                  type="text" 
                  name="NAME" 
                  id="mce-NAME"
                  className={`${Classes.INPUT} ${isTabletOrMobile ? Classes.FILL : ''}`}
                />
              </div>
              <div hidden><input type="hidden" name="tags" value="7199614,7199618" /></div>
              <div id="mce-responses">
                <div id="mce-error-response" style={{ display: 'none' }}></div>
                <div id="mce-success-response" style={{ display: 'none' }}></div>
              </div>
              <div style={{ position: 'absolute', left: -5000 }} aria-hidden="true">
                <input type="text" name="b_e12c33b9577c48d9fc701754d_487ca94ed4" tabIndex={-1} />
              </div>
              <Button type="submit" text="Subscribe" name="subscribe" id="mc-embedded-subscribe" />
            </div>
          </form>
        </div>
      </Card>
    </main>
  )
}