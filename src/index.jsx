import { lazy, Suspense } from 'react';
import { createRoot } from 'react-dom/client'
import { HashRouter } from 'react-router-dom'
import './index.less'

const App = lazy(() => import('./components/App'));

const root = createRoot(document.getElementById('app'));

root.render(
  <HashRouter>
    <Suspense fallback={null}><App /></Suspense>
  </HashRouter>
);