import { useMemo, useState } from 'react'

export default function useToggle(init) {
  const [value, setValue] = useState(init);

  const toggle = useMemo(
    () => {
      const fn = (override) => {
        if (typeof override === 'boolean') {
          setValue(override);
        } else {
          setValue((v) => !v);
        }
      };
      fn.FALSE = () => fn(false);
      fn.TRUE = () => fn(true);
      return fn;
    },
    []
  );

  return [value, toggle];
}