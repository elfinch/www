module.exports = [
  {
    title: 'Masquerade in London',
    brief: `
_The Samantha and Wyatt Mysteries, Book 1_

London, 1861. Samantha Kingston has lived under the control of her overbearing uncle since the death
of her parents six years ago. Trapped by the conventions of her gender and social class, she is
desperate to find a way out. But when her aunt and uncle are murdered, the freedom she sought seems
even further out of reach. A suspect in the crime, she finds herself on the run, unsure whom to
trust, lost in the underworld she had only ever read about in the pages of a Dickens novel.

V.T. Wyatt has defied his aristocratic upbringing to pursue a life as a private investigator. After
an unusual first meeting, he hires Samantha to help him solve a rash of burglaries. Soon, a
connection between the burglaries and the murders emerges, drawing Samantha and Wyatt even closer
together as they race to solve both mysteries and clear her name.
    `,
    links: [
      {
        href: 'https://www.amazon.com/Masquerade-London-Emily-L-Finch-ebook/dp/B09VDZZ1Y8',
        text: 'Buy on Amazon/Kindle',
        className: 'amz-button'
      },
      {
        href: 'https://www.audiobooks.com/promotions/promotedBook/704207/masquerade-in-london?refId=99701',
        text: 'Buy on Audiobooks.com',
        className: 'audiobooks-button'
      },
      {
        href: 'https://www.audible.com/pd/Masquerade-in-London-Audiobook/B0CJZQVSTK',
        text: 'Buy on Audible',
        className: 'amz-button'
      }  
    ],
    image: {
      src: 'Masquerade-in-London',
      alt: 'Masquerade in London',
      className: 'book-cover'
    },
  },
  {
    title: 'Ghosts in the Abbey',
    brief: `
_The Samantha and Wyatt Mysteries, Book 2_

**She can't escape her past...**

December 1861. Haunted by the events of the summer, Samantha Kingston has found her retreat to the
country less restful than she hoped. When a house party reintroduces V.T. Wyatt into her life after
a three-month absence, she has mixed feelings. It doesn't help that they must keep their distance
to stop the gossip mill running. But when Samantha stumbles across the body of someone she
recognizes and in whose death she fears she may have played a part, keeping their distance becomes
impossible. Working together once again, Samantha and Wyatt must solve a murder that grows more
complicated with every clue they uncover. But how does one find a killer who seems to have vanished
without a trace?

Intrigue and romance abound in this second installment of the Samantha and Wyatt Mysteries!
`,
    url: 'https://www.amazon.com/Ghosts-Abbey-Samantha-Wyatt-Mysteries-ebook/dp/B0BV17ZK61',
    links: [
      {
        href: 'https://www.amazon.com/Ghosts-Abbey-Samantha-Wyatt-Mysteries-ebook/dp/B0BV17ZK61',
        text: 'Buy on Amazon/Kindle',
        className: 'amz-button',
      },
      {
        href: 'https://www.audiobooks.com/audiobook/ghosts-in-the-abbey/704216',
        text: 'Buy on Audiobooks.com',
        className: 'audiobooks-button',
      },
      {
        href: 'https://www.audible.com/pd/Ghosts-in-the-Abbey-Audiobook/B0CMKF2433',
        text: 'Buy on Audible',
        className: 'amz-button',
      }
    ],
    image: {
      src: 'Ghosts-in-the-Abbey',
      alt: 'Ghosts in the Abbey',
      className: 'book-cover'
    },
  },
  {
    title: 'An Exhibition of Malice',
    brief: `
_The Samantha and Wyatt Mysteries, Book 3_

**Everything comes at a cost**

London, 1862. Now that London's most notorious criminal has set his sights on her, navigating the
scandal-mongers and fortune hunters of society is the least of Samantha's problems. Yet, even the
looming threat of Skinny Jim won't stop her from finding ways to put her new inheritance to good
use. But when one of her philanthropist friends is found dead in what police say was a robbery gone
wrong, she's certain he was murdered, and she thinks she knows who did it. To find out, she'll need
the help of the man whose proposal she rejected—a man she is beginning to think she can't live
without.

V.T. Wyatt would love nothing more than to win Samantha over, but first he needs to keep her
alive—which would be a lot easier if she didn't walk headlong into danger. As Samantha and Wyatt
find themselves drawn into a web of blackmail and deception, they must decide what price they are
willing to pay to protect their secrets.
`,
    url: 'https://www.amazon.com/Exhibition-Malice-Samantha-Wyatt-Mysteries-ebook/dp/B0CMGXQDXV',
    links: [
      {
        href: 'https://www.amazon.com/Exhibition-Malice-Samantha-Wyatt-Mysteries-ebook/dp/B0CMGXQDXV',
        text: 'Buy on Amazon/Kindle',
        className: 'amz-button',
      },
      {
        href: 'https://www.audiobooks.com/audiobook/exhibition-of-malice/820838',
        text: 'Buy on Audiobooks.com',
        className: 'audiobooks-button',
      },
      {
        href: 'https://www.audible.com/pd/An-Exhibition-of-Malice-Audiobook/B0DM43QL65',
        text: 'Buy on Audible',
        className: 'amz-button',
      }
    ],
    image: {
      src: 'an-Exhibition-of-Malice',
      alt: 'An Exhibition of Malice',
      className: 'book-cover'
    },
  }
]